package com.blog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blog.entity.Blog;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hyk
 * @since 2021-06-23
 */
public interface BlogMapper extends BaseMapper<Blog> {

}
