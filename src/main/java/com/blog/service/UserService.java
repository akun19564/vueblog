package com.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blog.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hyk
 * @since 2021-06-23
 */
public interface UserService extends IService<User> {

}
