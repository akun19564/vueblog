package com.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blog.entity.Blog;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author hyk
 * @since 2021-06-23
 */
public interface BlogService extends IService<Blog> {

}
